use crate::{error::KubectlWrapperError, KubectlWrapper};

/// Mock execute_raw method only
/// Other methods will return error
#[derive(Debug, Clone)]
pub struct MockRawKubectlWrapperImpl {
    /// If `true` returns `execute_raw_result` value
    /// Otherwise - KubectlWrapperError::Error
    execute_raw_success: bool,
    execute_raw_result: String,
}

impl MockRawKubectlWrapperImpl {
    pub fn new(execute_raw_success: bool, execute_raw_result: &str) -> Self {
        Self {
            execute_raw_success,
            execute_raw_result: execute_raw_result.to_string(),
        }
    }
}

impl KubectlWrapper for MockRawKubectlWrapperImpl {
    async fn get_resource_names(
        &self,
        _namespace: Option<String>,
        _resource_type: crate::KubernetesResourceType,
    ) -> Result<Vec<String>, crate::error::KubectlWrapperError> {
        Err(KubectlWrapperError::Error)
    }

    async fn execute_raw(
        &self,
        _args_row: &str,
    ) -> Result<String, crate::error::KubectlWrapperError> {
        if self.execute_raw_success {
            Ok(self.execute_raw_result.to_string())
        } else {
            Err(KubectlWrapperError::Error)
        }
    }
}
