use std::collections::HashMap;

use chrono::{DateTime, Local};

use crate::{
    deployment::model::KubeDeploymentPartialDescription, error::KubectlWrapperError,
    pod::model::PodConditions, KubectlWrapper,
};

#[derive(Debug, Clone)]
pub struct MockKubectlWrapperImpl {
    get_resource_names_success: bool,
    get_resource_names_result: Vec<String>,
    get_pod_names_success: bool,
    get_pod_names_result: Vec<String>,
    get_pod_status_success: bool,
    get_pod_status_result: String,
    get_pod_conditions_success: bool,
    get_pod_conditions_result: PodConditions,
    get_pod_start_time_success: bool,
    get_pod_start_time_result: DateTime<Local>,
    execute_raw_success: bool,
    execute_raw_result: String,
    get_deployment_description_success: bool,
    get_deployment_description_result: KubeDeploymentPartialDescription,
    get_configmap_key_values_success: bool,
    get_configmap_key_values_result: HashMap<String, String>,
    get_secret_key_values_success: bool,
    get_secret_key_values_result: HashMap<String, String>,
}

impl MockKubectlWrapperImpl {
    pub fn new(
        get_resource_names_success: bool,
        get_resource_names_result: &Vec<String>,
        get_pod_names_success: bool,
        get_pod_names_result: Vec<String>,
        get_pod_status_success: bool,
        get_pod_status_result: String,
        get_pod_conditions_success: bool,
        get_pod_conditions_result: &PodConditions,
        execute_raw_success: bool,
        execute_raw_result: &str,
        get_pod_start_time_success: bool,
        get_pod_start_time_result: DateTime<Local>,
        get_deployment_description_success: bool,
        get_deployment_description_result: &KubeDeploymentPartialDescription,
        get_configmap_key_values_success: bool,
        get_configmap_key_values_result: HashMap<String, String>,
        get_secret_key_values_success: bool,
        get_secret_key_values_result: HashMap<String, String>,
    ) -> Self {
        Self {
            get_resource_names_success,
            get_resource_names_result: get_resource_names_result.clone(),
            execute_raw_success,
            execute_raw_result: execute_raw_result.to_string(),
            get_deployment_description_success,
            get_deployment_description_result: get_deployment_description_result.clone(),
            get_pod_names_success,
            get_pod_names_result: get_pod_names_result.clone(),
            get_pod_status_success,
            get_pod_status_result: get_pod_status_result.to_string(),
            get_pod_conditions_success,
            get_pod_conditions_result: get_pod_conditions_result.clone(),
            get_pod_start_time_success,
            get_pod_start_time_result: get_pod_start_time_result.clone(),
            get_configmap_key_values_success,
            get_configmap_key_values_result: get_configmap_key_values_result.clone(),
            get_secret_key_values_success,
            get_secret_key_values_result: get_secret_key_values_result.clone(),
        }
    }
}

impl KubectlWrapper for MockKubectlWrapperImpl {
    async fn get_resource_names(
        &self,
        _namespace: Option<String>,
        _resource_type: crate::KubernetesResourceType,
    ) -> Result<Vec<String>, KubectlWrapperError> {
        if self.get_resource_names_success {
            Ok(self.get_resource_names_result.clone())
        } else {
            Err(KubectlWrapperError::Error)
        }
    }

    async fn execute_raw(&self, _args_row: &str) -> Result<String, KubectlWrapperError> {
        if self.execute_raw_success {
            Ok(self.execute_raw_result.clone())
        } else {
            Err(KubectlWrapperError::Error)
        }
    }

    #[cfg(feature = "deployments")]
    async fn get_deployment_description(
        &self,
        _namespace: &str,
        _deployment_name: &str,
    ) -> Result<KubeDeploymentPartialDescription, KubectlWrapperError> {
        if self.get_deployment_description_success {
            Ok(self.get_deployment_description_result.clone())
        } else {
            Err(KubectlWrapperError::Error)
        }
    }

    #[cfg(feature = "pods")]
    async fn get_pod_names(&self, _namespace: &str) -> Result<Vec<String>, KubectlWrapperError> {
        if self.get_pod_names_success {
            Ok(self.get_pod_names_result.clone())
        } else {
            Err(KubectlWrapperError::Error)
        }
    }

    #[cfg(feature = "pods")]
    async fn get_pod_status(
        &self,
        _namespace: &str,
        _pod_name: &str,
    ) -> Result<String, KubectlWrapperError> {
        if self.get_pod_status_success {
            Ok(self.get_pod_status_result.to_string())
        } else {
            Err(KubectlWrapperError::Error)
        }
    }

    #[cfg(feature = "pods")]
    async fn get_pod_conditions(
        &self,
        _namespace: &str,
        _pod_name: &str,
    ) -> Result<PodConditions, KubectlWrapperError> {
        if self.get_pod_conditions_success {
            Ok(self.get_pod_conditions_result.clone())
        } else {
            Err(KubectlWrapperError::Error)
        }
    }

    #[cfg(feature = "pods")]
    async fn get_pod_start_time(
        &self,
        _namespace: &str,
        _pod_name: &str,
    ) -> Result<DateTime<Local>, KubectlWrapperError> {
        if self.get_pod_start_time_success {
            Ok(self.get_pod_start_time_result.clone())
        } else {
            Err(KubectlWrapperError::Error)
        }
    }

    #[cfg(feature = "configmaps")]
    async fn get_configmap_key_values(
        &self,
        _namespace: &str,
        _name: &str,
    ) -> Result<HashMap<String, String>, KubectlWrapperError> {
        if self.get_configmap_key_values_success {
            Ok(self.get_configmap_key_values_result.clone())
        } else {
            Err(KubectlWrapperError::Error)
        }
    }

    #[cfg(feature = "secrets")]
    async fn get_secret_key_values(
        &self,
        _namespace: &str,
        _name: &str,
        _ignore_base64_errors: bool,
        _ignore_utf8_errors: bool,
    ) -> Result<HashMap<String, String>, KubectlWrapperError> {
        if self.get_secret_key_values_success {
            Ok(self.get_secret_key_values_result.clone())
        } else {
            Err(KubectlWrapperError::Error)
        }
    }
}
