use std::process::Command;
use std::{collections::HashMap, future::Future};

use base64::Engine;
use chrono::{DateTime, Local};
use deployment::model::KubeDeploymentPartialDescription;
use derive_more::Display;
use log::{debug, error, info, trace};
use pod::model::{KubePodPartialDescription, PodConditions};
use regex::Regex;

use crate::error::KubectlWrapperError;

pub mod deployment;
pub mod error;
pub mod pod;

#[cfg(test)]
mod tests;

#[cfg(feature = "mock")]
pub mod mock;

#[derive(Display, Debug, Clone)]
pub enum KubernetesResourceType {
    #[display(fmt = "pod")]
    Pod,

    #[display(fmt = "configmap")]
    ConfigMap,

    #[display(fmt = "secret")]
    Secret,

    #[display(fmt = "deployment")]
    Deployment,

    #[display(fmt = "statefulset")]
    StatefulSet,

    #[display(fmt = "replicaset")]
    ReplicaSet,

    #[display(fmt = "serviceaccount")]
    ServiceAccount,
}

pub trait KubectlWrapper: Send + Sync + Clone + 'static {
    fn get_resource_names(
        &self,
        namespace: Option<String>,
        resource_type: KubernetesResourceType,
    ) -> impl Future<Output = Result<Vec<String>, KubectlWrapperError>> + Send;

    #[cfg(feature = "configmaps")]
    fn get_configmap_key_values(
        &self,
        namespace: &str,
        name: &str,
    ) -> impl Future<Output = Result<HashMap<String, String>, KubectlWrapperError>> + Send;

    #[cfg(feature = "deployments")]
    fn get_deployment_description(
        &self,
        namespace: &str,
        deployment_name: &str,
    ) -> impl Future<Output = Result<KubeDeploymentPartialDescription, KubectlWrapperError>> + Send;

    #[cfg(feature = "pods")]
    /// Get pod names from given namespace.
    /// Return empty vec if there are no running pods.
    fn get_pod_names(
        &self,
        namespace: &str,
    ) -> impl Future<Output = Result<Vec<String>, KubectlWrapperError>> + Send;

    #[cfg(feature = "pods")]
    /// Return pod status string by given name.
    ///
    /// Return error for unknown pod.
    fn get_pod_status(
        &self,
        namespace: &str,
        pod_name: &str,
    ) -> impl Future<Output = Result<String, KubectlWrapperError>> + Send;

    #[cfg(feature = "pods")]
    /// Get pod conditions by given name.
    ///
    /// Conditions:
    ///  Type              Status
    ///  Initialized       True
    ///  Ready             True
    ///  ContainersReady   True
    ///  PodScheduled      True
    ///
    /// Return error for unknown pod.
    fn get_pod_conditions(
        &self,
        namespace: &str,
        pod_name: &str,
    ) -> impl Future<Output = Result<PodConditions, KubectlWrapperError>> + Send;

    #[cfg(feature = "pods")]
    /// Return pod start time.
    ///
    /// Return error for unknown pod.
    fn get_pod_start_time(
        &self,
        namespace: &str,
        pod_name: &str,
    ) -> impl Future<Output = Result<DateTime<Local>, KubectlWrapperError>> + Send;

    #[cfg(feature = "secrets")]
    fn get_secret_key_values(
        &self,
        namespace: &str,
        name: &str,
        ignore_base64_errors: bool,
        ignore_utf8_errors: bool,
    ) -> impl Future<Output = Result<HashMap<String, String>, KubectlWrapperError>> + Send;

    fn execute_raw(
        &self,
        args_row: &str,
    ) -> impl Future<Output = Result<String, KubectlWrapperError>> + Send;
}

/// Wrapper for `kubectl`
#[derive(Clone)]
pub struct KubectlWrapperImpl(bool);

impl KubectlWrapperImpl {
    pub fn safe_mode(&self) -> bool {
        self.0
    }
}

impl KubectlWrapper for KubectlWrapperImpl {
    async fn get_resource_names(
        &self,
        namespace: Option<String>,
        resource_type: KubernetesResourceType,
    ) -> Result<Vec<String>, KubectlWrapperError> {
        info!("get resource '{:?}' names", resource_type);

        let args = match namespace {
            Some(namespace) => {
                info!("namespace: '{namespace}'");
                format!("-n {} get {}", namespace, resource_type)
            }
            None => format!("-A get {}", resource_type),
        };

        let output = self.execute_raw(&args).await?;

        let mut header_found = false;

        let lines = output.split("\n").collect::<Vec<&str>>();

        let mut resource_names: Vec<String> = vec![];

        for line in lines {
            trace!("line: '{line}'");
            if header_found {
                let line_parts = line.split(" ").collect::<Vec<&str>>();

                match line_parts.first() {
                    Some(resource_name) => {
                        let resource_name = resource_name.trim();
                        if !resource_name.is_empty() {
                            debug!("name '{resource_name}'");
                            resource_names.push(resource_name.to_string());
                        }
                    }
                    None => {}
                }
            } else {
                if line.contains("NAME") {
                    header_found = true;
                }
            }
        }

        Ok(resource_names)
    }

    /// Execute raw command
    ///
    /// Other options will be ignored (namespace, etc.)
    async fn execute_raw(&self, args_row: &str) -> Result<String, KubectlWrapperError> {
        info!("execute kubectl command with args '{args_row}'..");

        let args: Vec<&str> = args_row.split(" ").collect();

        match Command::new("kubectl").args(args).output() {
            Ok(output) => {
                if output.status.success() {
                    let stdout = String::from_utf8(output.stdout)?;

                    let stdout = stdout.trim_matches('\'');

                    if !self.safe_mode() {
                        debug!("<stdout>");
                        debug!("{}", stdout);
                        debug!("</stdout>");
                    }

                    Ok(stdout.to_string())
                } else {
                    error!("command execution error");
                    let stderr = String::from_utf8_lossy(&output.stderr);

                    error!("<stderr>");
                    error!("{}", stderr);
                    error!("</stderr>");

                    Err(KubectlWrapperImpl::get_kind_of_error_by_stderr(&stderr))
                }
            }
            Err(e) => {
                error!("execution error: {}", e);
                Err(KubectlWrapperError::ExecutionError(e))
            }
        }
    }

    #[cfg(feature = "deployments")]
    async fn get_deployment_description(
        &self,
        namespace: &str,
        deployment_name: &str,
    ) -> Result<KubeDeploymentPartialDescription, KubectlWrapperError> {
        info!(
            "get deployment '{}' description at namespace '{}'",
            deployment_name, namespace
        );
        let args = format!("-n {} get deploy {} -o json", &namespace, deployment_name);
        let description_json = self.execute_raw(&args).await?;

        match serde_json::from_str::<KubeDeploymentPartialDescription>(&description_json) {
            Ok(description) => Ok(description),
            Err(e) => {
                error!("couldn't get deployment description: {}", e);
                Err(KubectlWrapperError::Error)
            }
        }
    }

    #[cfg(feature = "pods")]
    async fn get_pod_names(&self, namespace: &str) -> Result<Vec<String>, KubectlWrapperError> {
        info!("get pods names for namespace '{}'", &namespace);

        let args = format!("-n {} get pods", &namespace);

        let output = self.execute_raw(&args).await?;

        let mut header_found = false;

        let lines = output.split("\n").collect::<Vec<&str>>();

        let mut pod_names: Vec<String> = vec![];

        for line in lines {
            if header_found {
                let normalized_line = line.replace("  ", " ");
                debug!("normalized line: '{}'", normalized_line);
                let line_parts = normalized_line.split(" ").collect::<Vec<&str>>();

                match line_parts.first() {
                    Some(pod_name) => {
                        debug!("pod name '{}'", pod_name);
                        pod_names.push(pod_name.to_string());
                        break;
                    }
                    None => {}
                }
            } else {
                if line.contains("NAME") && line.contains("READY") {
                    header_found = true;
                }
            }
        }

        Ok(pod_names)
    }

    #[cfg(feature = "pods")]
    async fn get_pod_status(
        &self,
        namespace: &str,
        pod_name: &str,
    ) -> Result<String, KubectlWrapperError> {
        info!(
            "get pod '{}' status at namespace '{}'",
            pod_name, &namespace
        );
        let pod_description = self
            .execute_raw(&format!("-n {namespace} describe pod {pod_name}"))
            .await?;

        let mut pod_status = String::new();

        let pattern = Regex::new("(\\s+)Status:\\s+(?P<status>[a-zA-Z]+)")
            .expect("internal error, invalid regex pattern for pod status");

        for caps in pattern.captures_iter(&pod_description) {
            pod_status = caps["status"].to_string();
            break;
        }

        Ok(pod_status)
    }

    #[cfg(feature = "pods")]
    async fn get_pod_conditions(
        &self,
        namespace: &str,
        pod_name: &str,
    ) -> Result<PodConditions, KubectlWrapperError> {
        info!(
            "get pod '{}' (namespace '{}') conditions",
            pod_name, &namespace
        );

        let args = format!("-n {} describe pod {}", &namespace, pod_name);
        let pod_description = self.execute_raw(&args).await?;

        let initialized_value =
            self.get_pod_description_property_value(&pod_description, "Initialized");
        let ready_value = self.get_pod_description_property_value(&pod_description, "Ready");
        let containers_ready_value =
            self.get_pod_description_property_value(&pod_description, "ContainersReady");
        let pod_scheduled_value =
            self.get_pod_description_property_value(&pod_description, "PodScheduled");

        if initialized_value.is_some()
            && ready_value.is_some()
            && containers_ready_value.is_some()
            && pod_scheduled_value.is_some()
        {
            let pod_conditions = PodConditions {
                initialized: self
                    .get_description_boolean_value_from_str(&initialized_value.unwrap()),
                ready: self.get_description_boolean_value_from_str(&ready_value.unwrap()),
                containers_ready: self
                    .get_description_boolean_value_from_str(&containers_ready_value.unwrap()),
                pod_scheduled: self
                    .get_description_boolean_value_from_str(&pod_scheduled_value.unwrap()),
            };

            debug!("pod conditions:");
            debug!("{:?}", pod_conditions);

            Ok(pod_conditions)
        } else {
            error!(
                "couldn't get one or more condition property values for pod '{}'",
                pod_name
            );
            Err(KubectlWrapperError::Error)
        }
    }

    #[cfg(feature = "pods")]
    async fn get_pod_start_time(
        &self,
        namespace: &str,
        pod_name: &str,
    ) -> Result<DateTime<Local>, KubectlWrapperError> {
        info!("get pod '{pod_name}' start time, namespace '{namespace}'");

        let args = format!("-n {namespace} get pod {pod_name} -o json");
        let description_json = self.execute_raw(&args).await?;

        match serde_json::from_str::<KubePodPartialDescription>(&description_json) {
            Ok(description) => Ok(description.status.start_time),
            Err(e) => {
                error!("couldn't get pod start time: {}", e);
                Err(KubectlWrapperError::Error)
            }
        }
    }

    #[cfg(feature = "configmaps")]
    async fn get_configmap_key_values(
        &self,
        namespace: &str,
        name: &str,
    ) -> Result<HashMap<String, String>, KubectlWrapperError> {
        info!("get configmap '{name}' key-values (namespace '{namespace}')..");

        let output = &self
            .execute_raw(&format!(
                "get cm -n {namespace} {name} -o=jsonpath='{{.data}}'"
            ))
            .await?;
        let output = output.trim_matches('\n');

        match serde_json::from_str::<HashMap<String, String>>(&output) {
            Ok(results) => {
                debug!("config map values:");
                debug!("{:?}", results);
                Ok(results)
            }
            Err(e) => {
                error!("configmap data deserialization error: {}", e);
                Err(KubectlWrapperError::DeserializationError(e))
            }
        }
    }

    #[cfg(feature = "secrets")]
    async fn get_secret_key_values(
        &self,
        namespace: &str,
        name: &str,
        ignore_base64_errors: bool,
        ignore_utf8_errors: bool,
    ) -> Result<HashMap<String, String>, KubectlWrapperError> {
        info!("get secret '{name}' key-values (namespace '{namespace}')..");
        info!("ignore base64 errors: {ignore_base64_errors}");
        info!("ignore utf8 errors: {ignore_utf8_errors}");

        let output = &self
            .execute_raw(&format!(
                "get secret -n {namespace} {name} -o=jsonpath='{{.data}}'"
            ))
            .await?;
        let output = output.trim_matches('\n');

        info!("read key and values from secret..");

        match serde_json::from_str::<HashMap<String, String>>(&output) {
            Ok(encoded_results) => {
                if !&self.safe_mode() {
                    debug!("secret values:");
                    debug!("{:?}", encoded_results);
                }

                info!("decoding..");

                let mut secrets: HashMap<String, String> = HashMap::new();

                for (key, encoded_value) in encoded_results {
                    match base64::prelude::BASE64_STANDARD.decode(&encoded_value) {
                        Ok(decoded) => {
                            match String::from_utf8(decoded) {
                                Ok(value) => secrets.insert(key.to_string(), value),
                                Err(e) => {
                                    error!("secret '{key}' contains non-utf8 secret value: {}", e);

                                    if ignore_utf8_errors {
                                        info!("use value AS IS");
                                        secrets.insert(key.to_string(), encoded_value.to_string())
                                    } else {
                                        return Err(KubectlWrapperError::Error);
                                    }
                                }
                            };
                        }
                        Err(e) => {
                            error!("secret value '{key}' decoding error: {}", e);
                            if ignore_base64_errors {
                                info!("use value AS IS");
                                secrets.insert(key.to_string(), encoded_value.to_string());
                            } else {
                                return Err(KubectlWrapperError::Error);
                            }
                        }
                    }
                }

                Ok(secrets)
            }
            Err(e) => {
                error!("secrets data deserialization error: {}", e);
                Err(KubectlWrapperError::DeserializationError(e))
            }
        }
    }
}

impl KubectlWrapperImpl {
    /// Create an instance of `KubectlWrapper` in safe mode.
    /// Safe mode prevent to log any sensitive data
    pub fn new() -> Self {
        Self(true)
    }

    /// Create instance of `KubectlWrapper` with `default` namespace
    ///
    /// Logs all data, even secrets
    pub fn new_unsafe() -> Self {
        Self(false)
    }

    pub fn get_kind_of_error_by_stderr(stderr: &str) -> KubectlWrapperError {
        let mut error: KubectlWrapperError = KubectlWrapperError::Error;

        if stderr.contains("(NotFound)") {
            error = KubectlWrapperError::ResourceNotFound
        }

        error
    }

    fn get_description_boolean_value_from_str(&self, value: &str) -> bool {
        if value.to_lowercase() == "true" {
            true
        } else {
            false
        }
    }

    fn get_pod_description_property_value(
        &self,
        description: &str,
        property_name: &str,
    ) -> Option<String> {
        let expression = format!("(\\s+){}\\s+(?P<value>[a-zA-Z0-9]+)", property_name);

        let pattern = Regex::new(&expression)
            .expect("internal error, invalid regex pattern for pod property");

        let mut result: Option<String> = None;

        for caps in pattern.captures_iter(&description) {
            result = Some(caps["value"].to_string());
            break;
        }

        result
    }
}

#[cfg(test)]
mod get_pods_tests {
    use crate::tests::{get_random_string, init_logging};
    use crate::{KubectlWrapper, KubectlWrapperImpl, KubernetesResourceType};

    #[tokio::test]
    async fn return_pod_names() {
        init_logging();

        match KubectlWrapperImpl::new()
            .get_resource_names(Some("kube-system".to_string()), KubernetesResourceType::Pod)
            .await
        {
            Ok(pod_names) => {
                assert!(!pod_names.is_empty());
                let pod_name = pod_names.first().unwrap();
                assert!(pod_name.starts_with("coredns"));
            }
            Err(_) => panic!("stdout expected"),
        }
    }

    #[tokio::test]
    async fn return_empty_vec_for_unknown_namespace() {
        init_logging();
        let namespace: String = get_random_string();
        let names = KubectlWrapperImpl::new()
            .get_resource_names(Some(namespace), KubernetesResourceType::Pod)
            .await
            .unwrap();
        assert!(names.is_empty())
    }
}

#[cfg(test)]
mod execute_raw_tests {
    use crate::error::KubectlWrapperError;
    use crate::tests::{get_random_string, init_logging};
    use crate::{KubectlWrapper, KubectlWrapperImpl};

    #[tokio::test]
    async fn expect_valid_stdout() {
        init_logging();

        match KubectlWrapperImpl::new()
            .execute_raw("-n kube-system get pods")
            .await
        {
            Ok(stdout) => {
                assert!(stdout.contains("coredns"));
                assert!(stdout.contains("1/1"));
                assert!(stdout.contains("Running"));
            }
            Err(_) => panic!("stdout expected"),
        }
    }

    // NEGATIVE TESTS

    #[tokio::test]
    async fn return_error_for_invalid_args() {
        init_logging();
        assert!(KubectlWrapperImpl::new()
            .execute_raw("invalid-arg")
            .await
            .is_err())
    }

    #[tokio::test]
    async fn return_not_found_kind_error_for_unknown_resource() {
        init_logging();

        let namespace: String = get_random_string();

        let args = format!("delete ns {namespace}");
        match KubectlWrapperImpl::new().execute_raw(&args).await {
            Ok(_) => panic!("error expected"),
            Err(e) => match e {
                KubectlWrapperError::ResourceNotFound => assert!(true),
                _ => panic!("resource not found error expected"),
            },
        }
    }
}

#[cfg(feature = "deployments")]
#[cfg(test)]
mod get_resource_status_tests {
    use crate::tests::init_logging;
    use crate::{KubectlWrapper, KubectlWrapperImpl};

    #[tokio::test]
    async fn return_deployment_containers() {
        init_logging();

        let wrapper = KubectlWrapperImpl::new();

        match &wrapper.get_deployment_description("tests", "whoami").await {
            Ok(description) => {
                let container = &description.spec.template.spec.containers.first().unwrap();
                assert_eq!(container.name, "whoami");
                assert!(container.image.contains("traefik/whoami:v1.10.3"));
            }
            Err(_) => panic!("container expected"),
        }
    }

    #[tokio::test]
    async fn return_deployment_availability_status() {
        init_logging();

        let wrapper = KubectlWrapperImpl::new();

        match &wrapper.get_deployment_description("tests", "whoami").await {
            Ok(description) => {
                let result_status = description.status.get_availability_status().unwrap();
                assert!(result_status);
            }
            Err(_) => panic!("status string expected"),
        }
    }
}

#[cfg(feature = "pods")]
#[cfg(test)]
mod pod_method_tests {
    use crate::pod::model::PodConditions;
    use crate::tests::{get_random_string, init_logging};
    use crate::KubectlWrapper;
    use crate::KubectlWrapperImpl;

    #[tokio::test]
    async fn return_error_for_unknown_deployment() {
        init_logging();

        let wrapper = KubectlWrapperImpl::new();

        let name = get_random_string();

        assert!(&wrapper.get_pod_status("kube-system", &name).await.is_err())
    }

    #[tokio::test]
    async fn return_error_for_unknown_pod() {
        init_logging();

        let wrapper = KubectlWrapperImpl::new();

        let pod_name = get_random_string();

        assert!(&wrapper
            .get_pod_status("kube-system", &pod_name)
            .await
            .is_err())
    }

    #[tokio::test]
    async fn return_pod_conditions() {
        init_logging();

        let wrapper = KubectlWrapperImpl::new();

        let pod_names = &wrapper.get_pod_names("kube-system").await.unwrap();
        let pod_name = pod_names.first().unwrap();

        match &wrapper.get_pod_conditions("kube-system", pod_name).await {
            Ok(conditions) => {
                let expected_conditions = PodConditions {
                    initialized: true,
                    ready: true,
                    containers_ready: true,
                    pod_scheduled: true,
                };

                assert_eq!(&expected_conditions, conditions);
            }
            Err(_) => panic!("result expected"),
        }
    }

    #[tokio::test]
    async fn datetime_should_be_returned() {
        init_logging();

        let wrapper = KubectlWrapperImpl::new();

        let pod_names = wrapper.get_pod_names("kube-system").await.unwrap();

        let pod_name = pod_names
            .iter()
            .find(|name| name.starts_with("coredns"))
            .unwrap();

        assert!(KubectlWrapperImpl::new()
            .get_pod_start_time("kube-system", pod_name)
            .await
            .is_ok())
    }
}

#[cfg(feature = "configmaps")]
#[cfg(test)]
mod config_map_tests {
    use std::collections::HashMap;

    use crate::{KubectlWrapper, KubectlWrapperImpl};

    #[tokio::test]
    async fn get_configmap_key_and_values() {
        let kubectl = KubectlWrapperImpl::new();

        let results = kubectl
            .get_configmap_key_values("tests", "test-cm")
            .await
            .unwrap();

        let expected_results: HashMap<String, String> = HashMap::from([
            ("LOG_LEVEL".to_string(), "debug".to_string()),
            ("TEST".to_string(), "value".to_string()),
            ("NAME".to_string(), "Joe".to_string()),
        ]);

        for (expected_key, expected_value) in expected_results {
            assert!(results.contains_key(&expected_key));

            let value = results.get(&expected_key).unwrap();

            assert_eq!(value, &expected_value);
        }
    }
}

#[cfg(feature = "secrets")]
#[cfg(test)]
mod secret_tests {
    use std::collections::HashMap;

    use crate::{tests::init_logging, KubectlWrapper, KubectlWrapperImpl};

    #[tokio::test]
    async fn read_secrets() {
        init_logging();

        let wrapper = KubectlWrapperImpl::new();

        let results = wrapper
            .get_secret_key_values("tests", "test-secret", true, true)
            .await
            .unwrap();

        let expected_values: HashMap<String, String> = HashMap::from([
            ("USERNAME".to_string(), "username".to_string()),
            ("PASSWORD".to_string(), "password".to_string()),
            ("api-key".to_string(), "q34jf093j4f34f".to_string()),
        ]);

        for (expected_key, expected_value) in expected_values {
            assert!(results.contains_key(&expected_key));
            let result_value = results.get(&expected_key).unwrap();
            assert_eq!(result_value, &expected_value);
        }
    }
}
