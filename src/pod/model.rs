use chrono::{DateTime, Local};
use serde::Deserialize;

#[derive(PartialEq, Debug, Clone)]
pub struct PodStatus {
    pub phase: String,
    pub ready: bool,
    pub containers_ready: bool,
    pub pod_scheduled: bool,
}

/// Conditions:
///   Type              Status
///   Initialized       True
///   Ready             True
///   ContainersReady   True
///   PodScheduled      True
#[derive(PartialEq, Debug, Clone)]
pub struct PodConditions {
    pub initialized: bool,
    pub ready: bool,
    pub containers_ready: bool,
    pub pod_scheduled: bool,
}

/// Include partial fields of pod description.
///
/// Section:
///
/// - `status`
#[derive(PartialEq, Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct KubePodPartialDescription {
    pub status: KubePodDescriptionStatus,
}

/// Include partial fields of pod status.
///
/// Fields:
///
/// - `startTime`
#[derive(PartialEq, Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct KubePodDescriptionStatus {
    pub start_time: DateTime<Local>,
}
