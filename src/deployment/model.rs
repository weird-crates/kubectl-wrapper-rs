use serde::Deserialize;

/// Include partial fields of deployment description.
#[derive(PartialEq, Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct KubeDeploymentPartialDescription {
    pub spec: KubeDeploymentDescriptionSpec,
    pub status: KubeDeploymentDescriptionStatus,
}

#[derive(PartialEq, Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct KubeDeploymentDescriptionSpec {
    pub replicas: u16,

    pub template: KubeDeploymentDescriptionSpecTemplate,
}

#[derive(PartialEq, Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct KubeDeploymentDescriptionSpecTemplate {
    pub spec: KubeDeploymentDescriptionTemplateSpec,
}

#[derive(PartialEq, Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct KubeDeploymentDescriptionTemplateSpec {
    pub containers: Vec<KubeDeploymentDescriptionContainer>,
}

#[derive(PartialEq, Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct KubeDeploymentDescriptionContainer {
    pub name: String,
    pub image: String,
}

#[derive(PartialEq, Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct KubeDeploymentDescriptionStatus {
    pub conditions: Vec<KubeDeploymentDescriptionStatusCondition>,
}

impl KubeDeploymentDescriptionStatus {
    /// {
    ///   "lastTransitionTime": "2022-10-09T05:10:10Z",
    ///   "lastUpdateTime": "2022-10-09T05:10:10Z",
    ///   "message": "Deployment has minimum availability.",
    ///   "reason": "MinimumReplicasAvailable",
    ///   "status": "True",
    ///   "type": "Available"
    /// }
    pub fn get_availability_status(&self) -> Option<bool> {
        let condition_found = self
            .conditions
            .iter()
            .find(|condition| condition.condition_type == "Available");

        match condition_found {
            Some(condition) => {
                let condition_status = &condition.status;

                Some(match condition_status.as_ref() {
                    "True" => true,
                    _ => false,
                })
            }
            None => None,
        }
    }
}
#[derive(PartialEq, Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct KubeDeploymentDescriptionStatusCondition {
    pub status: String,

    #[serde(alias = "type")]
    pub condition_type: String,
}
