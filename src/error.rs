use std::string::FromUtf8Error;

use thiserror::Error;

#[derive(Error, Debug)]
pub enum KubectlWrapperError {
    #[error("Command execution error")]
    ExecutionError(#[from] std::io::Error),

    #[error("Deserialization error")]
    DeserializationError(#[from] serde_json::Error),

    #[error("UTF8 error")]
    Utf8Error(#[from] FromUtf8Error),

    #[error("Target resource wasn't found")]
    ResourceNotFound,

    #[error("Command execution error")]
    Error,
}
