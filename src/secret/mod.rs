use std::collections::HashMap;

use base64::Engine;
use log::{debug, error, info};

use crate::error::KubectlWrapperError;
use crate::KubectlWrapperImpl;

pub trait KubectlSecretWrapper {
    fn get_secret_key_values(
        &self,
        namespace: &str,
        name: &str,
        ignore_base64_errors: bool,
        ignore_utf8_errors: bool,
    ) -> Result<HashMap<String, String>, KubectlWrapperError>;
}

impl KubectlSecretWrapper for KubectlWrapperImpl {
    fn get_secret_key_values(
        &self,
        namespace: &str,
        name: &str,
        ignore_base64_errors: bool,
        ignore_utf8_errors: bool,
    ) -> Result<HashMap<String, String>, KubectlWrapperError> {
        info!("get secret '{name}' key-values (namespace '{namespace}')..");
        info!("ignore base64 errors: {ignore_base64_errors}");
        info!("ignore utf8 errors: {ignore_utf8_errors}");

        let output = &self.executor.execute(&format!(
            "get secret -n {namespace} {name} -o=jsonpath='{{.data}}'"
        ))?;
        let output = output.trim_matches('\n');

        info!("read key and values from secret..");

        match serde_json::from_str::<HashMap<String, String>>(&output) {
            Ok(encoded_results) => {
                if !&self.safe_mode {
                    debug!("secret values:");
                    debug!("{:?}", encoded_results);
                }

                info!("decoding..");

                let mut secrets: HashMap<String, String> = HashMap::new();

                for (key, encoded_value) in encoded_results {
                    match base64::prelude::BASE64_STANDARD.decode(&encoded_value) {
                        Ok(decoded) => {
                            match String::from_utf8(decoded) {
                                Ok(value) => secrets.insert(key.to_string(), value),
                                Err(e) => {
                                    error!("secret '{key}' contains non-utf8 secret value: {}", e);

                                    if ignore_utf8_errors {
                                        info!("use value AS IS");
                                        secrets.insert(key.to_string(), encoded_value.to_string())
                                    } else {
                                        return Err(KubectlWrapperError::Error);
                                    }
                                }
                            };
                        }
                        Err(e) => {
                            error!("secret value '{key}' decoding error: {}", e);
                            if ignore_base64_errors {
                                info!("use value AS IS");
                                secrets.insert(key.to_string(), encoded_value.to_string());
                            } else {
                                return Err(KubectlWrapperError::Error);
                            }
                        }
                    }
                }

                Ok(secrets)
            }
            Err(e) => {
                error!("secrets data deserialization error: {}", e);
                Err(KubectlWrapperError::DeserializationError(e))
            }
        }
    }
}
