# For developers

## Run tests

Define `KUBECONFIG` environment variable before tests execution.

Example for K3s:

```shell
export KUBECONFIG=/etc/rancher/k3s/k3s.yaml

kubectl apply -f test-entities.yaml

helm repo add cowboysysop https://cowboysysop.github.io/charts/
helm repo update
helm install -n tests whoami cowboysysop/whoami

cargo test
```
