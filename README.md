# kubectl wrapper for Rust

**Precaution:** experimental crate

Add dependency:

```toml
[dependencies]
kubectl-wrapper-rs = { git = "https://gitlab.com/weird-crates/kubectl-wrapper-rs", version = "0.17.0" }
```

Then use:

```rust
let wrapper = KubectlWrapperImpl::new();

// Get pod names for namespace `whoami`
let pod_names: Vec<String> = wrapper.get_pod_names("tests").await?;

// Get pod status
let pod_status: String = wrapper.get_pod_status("tests", "whoami").await?;

// Get pod conditions (Ready, ..)
//
// Conditions:
//  Type              Status
//  Initialized       True
//  Ready             True
//  ContainersReady   True
//  PodScheduled      True
let pod_conditions: PodConditions = wrapper.get_pod_conditions("tests", "whoami").await?;

// Get deployment status
let deployment_status: KubeDeploymentDescriptionStatus = wrapper.get_deployment_status("tests", "whoami").await?;

// Get deployment available status
let deployment_available: bool = &deployment_status.get_availability_status("tests");

// Get start time by given pod name
let pod_start_time = wrapper.get_pod_start_time("tests", "whoami").await?;

// Raw commands
let pods_list: String = wrapper.execute_raw("-n tests get pods").await?;
```

## Features

- `pods`
- `configmaps`
- `secrets`
- `deployments`
- `mock`

## Safety

All structs uses safety mode by default (don't log any output data). You can override it with unsafe constructors.

```rust
KubectlWrapperImpl::new_unsafe()
```
